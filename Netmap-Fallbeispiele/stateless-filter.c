/*
 * This program forwards packets between two netmap ports, an external
 * port (ext) and an internal port (int). Packets flowing from ext to
 * int are filtered according to the rules provided through command
 * line. Packets flowing from int to ext bypass the filter, and they
 * are always forwarded.
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <errno.h>
#include <poll.h>
#include <net/if.h>
#include <stdint.h>
#include <net/netmap.h>
#define NETMAP_WITH_LIBS
#include <net/netmap_user.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/if_ether.h>
#include <netinet/ip.h>
#include <netinet/udp.h>
#include <netinet/tcp.h>
#include <assert.h>
#include <arpa/inet.h>




/* You can undef MULTIRING to get the simpler code, which assumes
 * that each netmap port has a single RX ring and a single TX ring. */
//#define MULTIRING

static int			stop = 0;
static unsigned long long	fwd = 0;
static unsigned long long	tot = 0;

static void
sigint_handler(int signum)
{
	stop = 1;
}

struct filtrule {
	/* All fields are in network order. */
	uint32_t ip_daddr;
	uint32_t ip_mask;
	uint16_t dport;
	uint8_t ip_proto;
	uint8_t pad;
};

static inline int
pkt_print(const char *buf)
{
	struct ether_header *ethh;
	struct ip *iph;
	struct udphdr *udph;	
	struct tcphdr *tcph;
	int i;

	ethh = (struct ether_header *)buf;
	if (ethh->ether_type != htons(ETHERTYPE_IP)) {
		
	 // printf("found non-IP traffic\n");
	  	//printf("______________________________________________\n");
	  	//printf("\n");
		return 0;
	}

	iph = (struct ip *)(ethh + 1);
	udph = (struct udphdr *)(iph + 1);
	tcph = (struct tcphdr *)(iph + 1);
	char *contents = (char *) (udph + 1);
	
	// TCP
	if(iph->ip_p == 17) {

	tot++;
	//printf("====\n TCP PAKET Number: %d\n", tot);
	printf("====\n UDP PAKET Number: %d\n", tot);


	// IP ok
	
	printf("IP total length: %d\n", iph->ip_len);
	printf("IP protocol: %d\n", iph->ip_p);

	char saddr[INET_ADDRSTRLEN];
	inet_ntop(AF_INET, &(iph->ip_src.s_addr), saddr, INET_ADDRSTRLEN);
	printf("IP source address: %s\n", saddr);

	char daddr[INET_ADDRSTRLEN];
	inet_ntop(AF_INET, &(iph->ip_dst.s_addr), daddr, INET_ADDRSTRLEN);
	printf("IP destination address: %s\n", daddr);	
	
	
	// UDP
	
	printf("UDP source port: %d\n", ntohs(udph->uh_sport));
	printf("UDP destination port: %d\n", ntohs(udph->uh_dport));
	printf("UDP length: %d\n", ntohs(udph->uh_ulen));
	printf("UDP checksum : %d\n", ntohs(udph->uh_sum));

	//#printf("IP header length: %d", ntohs(udph->uh_ulen));
	//#PrintData(buf+(20*8)+(sizeof(udph)), (sizeof(char) * 100));

	PrintData(contents, (sizeof(char) * 100));
	//


	  

	// TCP
	
	printf("TCP source port: %d\n", ntohs(tcph->th_sport));
	printf("TCP destination port: %d\n",ntohs(tcph->th_dport));
	printf("TCP sequence number: %d\n", ntohl(tcph->th_seq));
	printf("TCP acknowledgement number: %d\n", ntohl(tcph->th_ack));
	printf("______________________________________________\n");	
	
	
	printf("\n");
        }



	



}


static void
forward_pkts(struct nm_desc *src)
{

          

	struct netmap_ring *rxring;
	unsigned int rxhead;

	rxring = NETMAP_RXRING(src->nifp, 0);
	

	for (rxhead = rxring->head; rxhead != rxring->tail; rxhead = nm_ring_next(rxring, rxhead)) {

		struct netmap_slot *rs = &rxring->slot[rxhead];		
		char *rxbuf            = NETMAP_BUF(rxring, rs->buf_idx);

		pkt_print(rxbuf);

		//printf("total: %d\n", tot);
		//tot++;
	}
	/* Update the pointers in the netmap rings. */
	rxring->head = rxring->cur = rxhead;
	

}



static inline int
rx_ready(struct nm_desc *nmd)
{

	return nm_ring_space(NETMAP_RXRING(nmd->nifp, 0));

}


static int
main_loop(const char *ext_port_name)
{

	//printf("main loop\n");

	struct nm_desc *ext_port;
	


	ext_port = nm_open(ext_port_name, NULL, 0, NULL);
	if (ext_port == NULL) {
		printf("Failed to nm_open(%s): not a netmap port\n", ext_port_name);		
		return -1;	
	}

	
	
	//printf("\n starting while loop...");

	while (!stop) {
		struct pollfd pfd[2];
		int ret;

		pfd[0].fd     = ext_port->fd; // pfd = poll file descriptor		
		pfd[0].events = 0;
	
		if (!rx_ready(ext_port)) {
		// Ran out of input packets on the first port, we need to wait for them.
		pfd[0].events |= POLLIN;
		} 
	

		/* We poll with a timeout to have a chance to break the main loop if
		 * no packets are coming. */
		ret = poll(pfd, 2, 1000);
		if (ret < 0) {
			perror("poll()");
		} else if (ret == 0) {
			/* Timeout */
			continue;
		}

		/* Forward in the two directions. */
		forward_pkts(ext_port);
		
	}

	nm_close(ext_port);
	

	//printf("Total processed packets: %llu\n", tot);


	return 0;
}



int
main(int argc, char **argv)
{

	printf("main\n");

	//const char *ext_port_name = "netmap:enp0s31f6";
	//const char *ext_port_name = "netmap:enp6s0";
        const char *ext_port_name = "netmap:wlp2s0";


	main_loop(ext_port_name);




//----






	return 0;
}


void PrintData (const u_char * data , int Size)
{
    int i , j;
    for(i=0 ; i < Size ; i++)
    {
        if( i!=0 && i%16==0)   //if one line of hex printing is complete...
        {
            printf("         ");
            for(j=i-16 ; j<i ; j++)
            {
                if(data[j]>=32 && data[j]<=128)
                    printf("%c",(unsigned char)data[j]); //if its a number or alphabet
                 
                else printf("."); //otherwise print a dot
            }
            printf("\n");
        } 
         
        if(i%16==0) printf("   ");
            printf(" %02X",(unsigned int)data[i]);
                 
        if( i==Size-1)  //print the last spaces
        {
            for(j=0;j<15-i%16;j++) 
            {
              printf("   "); //extra spaces
            }
             
            printf("         ");
             
            for(j=i-i%16 ; j<=i ; j++)
            {
                if(data[j]>=32 && data[j]<=128) 
                {
                  printf("%c",(unsigned char)data[j]);
                }
                else
                {
                  printf(".");
                }
            }
             
            printf("\n" );
        }
    }
}


