/*
 * This program forwards packets between two netmap ports, an external
 * port (ext) and an internal port (int). Packets flowing from ext to
 * int are filtered according to the rules provided through command
 * line. Packets flowing from int to ext bypass the filter, and they
 * are always forwarded.
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <errno.h>
#include <poll.h>
#include <net/if.h>
#include <stdint.h>
#include <net/netmap.h>
#define NETMAP_WITH_LIBS
#include <net/netmap_user.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/if_ether.h>
#include <netinet/ip.h>
#include <netinet/udp.h>
#include <netinet/tcp.h>
#include <assert.h>
#include <arpa/inet.h>

/* You can undef MULTIRING to get the simpler code, which assumes
 * that each netmap port has a single RX ring and a single TX ring. */
#define MULTIRING

static int			stop = 0;
static unsigned long long	fwd = 0;
static unsigned long long	tot = 0;

static void
sigint_handler(int signum)
{
	stop = 1;
}

/*
static void
sigint_handler(int signum)
{
	stop = 1;
}*/

struct filtrule {
	/* All fields are in network order. */
	uint32_t ip_daddr;
	uint32_t ip_mask;
	uint16_t dport;
	uint8_t ip_proto;
	uint8_t pad;
};

static inline int
pkt_select(const char *buf, struct filtrule *rules, int num_rules)
{
	struct ether_header *ethh;
	struct udphdr *udph;
	struct ip *iph;
	int i;

	ethh = (struct ether_header *)buf;
	if (ethh->ether_type != htons(ETHERTYPE_IP)) {
		/* Filter out non-IP traffic. */
		return 0;
	}
	iph = (struct ip *)(ethh + 1);
	udph = (struct udphdr *)(iph + 1);

	for (i = 0; i < num_rules; i++) {
		struct filtrule *rule = rules + i;

		if ((iph->ip_dst.s_addr & rule->ip_mask)
			== rule->ip_daddr &&
			(!rules->ip_proto || rule->ip_proto == iph->ip_p) &&
			(!rule->dport || rule->dport == udph->uh_dport)) {
			return 1; /* select */
		}
	}

	return 0; /* discard */
}

static void
forward_pkts(struct nm_desc *src, struct nm_desc *dst, struct filtrule *rules,
		int num_rules, int zerocopy)
{

	struct netmap_ring *txring;
	struct netmap_ring *rxring;
	unsigned int rxhead, txhead;

	rxring = NETMAP_RXRING(src->nifp, 0);
	txring = NETMAP_TXRING(dst->nifp, 0);

	for (rxhead = rxring->head, txhead = txring->head;
			rxhead != rxring->tail && txhead != txring->tail;
				tot++, rxhead = nm_ring_next(rxring, rxhead)) {
		struct netmap_slot *rs = &rxring->slot[rxhead];
		struct netmap_slot *ts = &txring->slot[txhead];
		char *rxbuf            = NETMAP_BUF(rxring, rs->buf_idx);

		if (rules && !pkt_select(rxbuf, rules, num_rules)) {
			continue; /* discard */
		}

		ts->len = rs->len;
		if (zerocopy) {
			uint32_t idx = ts->buf_idx;
			ts->buf_idx  = rs->buf_idx;
			rs->buf_idx  = idx;
			/* report the buffer change. */
			ts->flags |= NS_BUF_CHANGED;
			rs->flags |= NS_BUF_CHANGED;
		} else {
			char *txbuf = NETMAP_BUF(txring, ts->buf_idx);
			memcpy(txbuf, rxbuf, ts->len);
		}
		txhead = nm_ring_next(txring, txhead);
		fwd++;
	}
	/* Update the pointers in the netmap rings. */
	rxring->head = rxring->cur = rxhead;
	txring->head = txring->cur = txhead;

}

static inline int
rx_ready(struct nm_desc *nmd)
{
	return nm_ring_space(NETMAP_RXRING(nmd->nifp, 0));
}

static int
main_loop(const char *ext_port_name, const char *int_port_name,
		struct filtrule *rules, int num_rules, int force_copy)
{
	struct nm_desc *ext_port;
	struct nm_desc *int_port;
	int zerocopy;


	ext_port = nm_open(ext_port_name, NULL, 0, NULL);
	if (ext_port == NULL) {	
		printf("Failed to nm_open(%s): not a netmap port\n", ext_port_name);
		return -1;
	}

	int_port = nm_open(int_port_name, NULL, NM_OPEN_NO_MMAP, ext_port);
	if (int_port == NULL) {
		printf("Failed to nm_open(%s): not a netmap port\n",int_port_name);
		return -1;
	}

	/* Check if we can do zerocopy. */
	zerocopy = !force_copy && (ext_port->mem == int_port->mem);
	printf("zerocopy %sabled\n", zerocopy ? "en" : "dis");

	while (!stop) {
		struct pollfd pfd[2];
		int ret;

		pfd[0].fd     = ext_port->fd;
		pfd[1].fd     = int_port->fd;
		pfd[0].events = 0;
		pfd[1].events = 0;
		if (!rx_ready(ext_port)) {
			/* Ran out of input packets on the first port, we need to
			 * wait for them. */
			pfd[0].events |= POLLIN;
		} else {
			/* We have input packets on the first port, let's wait for
			 * TX ring space in the other port. */
			pfd[1].events |= POLLOUT;
		}
		if (!rx_ready(int_port)) {
			/* Ran out of input packets on the second port, we need to
			 * wait for them. */
			pfd[1].events |= POLLIN;
		} else {
			/* We have input packets on the second port, let's wait for
			 * TX ring space in the other port. */
			pfd[0].events |= POLLOUT;
		}

		/* We poll with a timeout to have a chance to break the main loop if
		 * no packets are coming. */
		ret = poll(pfd, 2, 1000);
		if (ret < 0) {
			perror("poll()");
		} else if (ret == 0) {
			/* Timeout */
			continue;
		}

		/* Forward in the two directions. */
		forward_pkts(ext_port, int_port, rules, num_rules, zerocopy);
		forward_pkts(int_port, ext_port, NULL, 0, zerocopy);
	}

	nm_close(ext_port);
	nm_close(int_port);

	printf("Total processed packets: %llu\n", tot);
	printf("Forwarded packets      : %llu\n", fwd);
	printf("Dropped packets        : %llu\n", tot - fwd);

	return 0;
}



int
main(int argc, char **argv)
{
#define MAXRULES 16
	struct filtrule rules[MAXRULES];
	

	struct sigaction sa;
	int force_copy = 0;
	int num_rules = 0;
	int opt, ret, i;

	const char *ext_port_name = "netmap:wlp2s0";
	const char *int_port_name = "netmap:wlp2s0";

	char *ipstr;
	int port, proto, mask;
	struct in_addr ip;

	ipstr = strtok("10.10.10.0", "/");	
	ret = inet_pton(AF_INET, ipstr, &ip);	
	mask = atoi("24");	
	proto = atoi("0");	
	port = atoi("7777");


	



	

	while ((opt = getopt(argc, argv, "h:p:c")) != -1) {
		switch (opt) {
		case 'h':
			
			return 0;



		case 'p': {
			char *copy = strdup(optarg);
	
			
			assert(copy != NULL);


			

			free(copy);



			rules[num_rules].ip_mask =
				(((uint64_t)1ULL << mask) - 1ULL) << (32 - mask);
			rules[num_rules].ip_mask = htonl(rules[num_rules].ip_mask);
			rules[num_rules].ip_daddr = ip.s_addr & rules[num_rules].ip_mask;
			rules[num_rules].ip_proto = proto;
			rules[num_rules].dport = htons(port);
			num_rules++;
			break;
		}

		case 'c':
			force_copy = 1;
			break;

		default:
			printf("    unrecognized option '-%c'\n", opt);
			
			return -1;
		}
	}



	/* Register Ctrl-C handler. */
	sa.sa_handler = sigint_handler;
	sigemptyset(&sa.sa_mask);
	sa.sa_flags = SA_RESTART;
	ret         = sigaction(SIGINT, &sa, NULL);
	if (ret) {
		perror("sigaction(SIGINT)");
		exit(EXIT_FAILURE);
	}

	printf("External port: %s\n", ext_port_name);
	printf("Internal port: %s\n", int_port_name);

	printf("Rules:\n");
	for (i = 0; i < num_rules; i++) {
		printf("    pass ip_daddr 0x%08x/0x%08x ip_proto %u "
			"dport %u\n",
			ntohl(rules[i].ip_daddr), ntohl(rules[i].ip_mask),
			rules[i].ip_proto, ntohs(rules[i].dport));
	}

	main_loop(ext_port_name, int_port_name, rules, num_rules, force_copy);

	return 0;
}
