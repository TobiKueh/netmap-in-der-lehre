# Automatically generated using Clang.jl


const NETMAP_DEVICE_NAME = "/dev/netmap"

# Skipping MacroDefinition: ND ( _fmt , ... ) do { } while ( 0 )
# Skipping MacroDefinition: D ( _fmt , ... ) do { struct timeval _t0 ; gettimeofday ( & _t0 , NULL ) ; fprintf ( stderr , "%03d.%06d %s [%d] " _fmt "\n" , ( int ) ( _t0 . tv_sec % 1000 ) , ( int ) _t0 . tv_usec , __FUNCTION__ , __LINE__ , ## __VA_ARGS__ ) ; } while ( 0 )
# Skipping MacroDefinition: RD ( lps , format , ... ) do { static int __t0 , __cnt ; struct timeval __xxts ; gettimeofday ( & __xxts , NULL ) ; if ( __t0 != __xxts . tv_sec ) { __t0 = __xxts . tv_sec ; __cnt = 0 ; } if ( __cnt ++ < lps ) { D ( format , ## __VA_ARGS__ ) ; } } while ( 0 )

const NM_MORE_PKTS = 1
const NM_ERRBUF_SIZE = 512

# Skipping MacroDefinition: P2NMD ( p ) ( ( struct nm_desc * ) ( p ) )
# Skipping MacroDefinition: IS_NETMAP_DESC ( d ) ( ( d ) && P2NMD ( d ) -> self == P2NMD ( d ) )
# Skipping MacroDefinition: NETMAP_FD ( d ) ( P2NMD ( d ) -> fd )

const MAXERRMSG = 80

struct nm_stat
    ps_recv::u_int
    ps_drop::u_int
    ps_ifdrop::u_int
end

struct nm_desc
    self::Ptr{nm_desc}
    fd::Cint
    mem::Ptr{Cvoid}
    memsize::Csize_t
    done_mmap::Cint
    nifp::Ptr{netmap_if}
    first_tx_ring::UInt16
    last_tx_ring::UInt16
    cur_tx_ring::UInt16
    first_rx_ring::UInt16
    last_rx_ring::UInt16
    cur_rx_ring::UInt16
    req::nmreq
    hdr::nm_pkthdr
    some_ring::Ptr{netmap_ring}
    buf_start::Ptr{Cvoid}
    buf_end::Ptr{Cvoid}
    snaplen::Cint
    promisc::Cint
    to_ms::Cint
    errbuf::Cstring
    if_flags::UInt32
    if_reqcap::UInt32
    if_curcap::UInt32
    st::nm_stat
    msg::NTuple{512, UInt8}
end

struct nm_pkthdr
    ts::timeval
    caplen::UInt32
    len::UInt32
    flags::UInt64
    d::Ptr{nm_desc}
    slot::Ptr{netmap_slot}
    buf::Ptr{UInt8}
end

const nm_cb_t = Ptr{Cvoid}
