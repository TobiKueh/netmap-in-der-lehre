# Julia wrapper for header: netmap_user.h
# Automatically generated using Clang.jl


function nm_ring_next(r, i)
    ccall((:nm_ring_next, libnetmap_user), UInt32, (Ptr{netmap_ring}, UInt32), r, i)
end

function nm_tx_pending(r)
    ccall((:nm_tx_pending, libnetmap_user), Cint, (Ptr{netmap_ring},), r)
end

function nm_ring_space(ring)
    ccall((:nm_ring_space, libnetmap_user), UInt32, (Ptr{netmap_ring},), ring)
end

function nm_pkt_copy(_src, _dst, l)
    ccall((:nm_pkt_copy, libnetmap_user), Cvoid, (Ptr{Cvoid}, Ptr{Cvoid}, Cint), _src, _dst, l)
end

function nm_open(ifname, req, flags, arg)
    ccall((:nm_open, libnetmap_user), Ptr{nm_desc}, (Cstring, Ptr{nmreq}, UInt64, Ptr{nm_desc}), ifname, req, flags, arg)
end

function nm_close(arg1)
    ccall((:nm_close, libnetmap_user), Cint, (Ptr{nm_desc},), arg1)
end

function nm_mmap(arg1, arg2)
    ccall((:nm_mmap, libnetmap_user), Cint, (Ptr{nm_desc}, Ptr{nm_desc}), arg1, arg2)
end

function nm_inject(arg1, arg2, arg3)
    ccall((:nm_inject, libnetmap_user), Cint, (Ptr{nm_desc}, Ptr{Cvoid}, Csize_t), arg1, arg2, arg3)
end

function nm_dispatch(arg1, arg2, arg3, arg4)
    ccall((:nm_dispatch, libnetmap_user), Cint, (Ptr{nm_desc}, Cint, nm_cb_t, Ptr{u_char}), arg1, arg2, arg3, arg4)
end

function nm_nextpkt(arg1, arg2)
    ccall((:nm_nextpkt, libnetmap_user), Ptr{u_char}, (Ptr{nm_desc}, Ptr{nm_pkthdr}), arg1, arg2)
end

function nm_is_identifier(s, e)
    ccall((:nm_is_identifier, libnetmap_user), Cint, (Cstring, Cstring), s, e)
end

function nm_parse(ifname, d, err)
    ccall((:nm_parse, libnetmap_user), Cint, (Cstring, Ptr{nm_desc}, Cstring), ifname, d, err)
end

function nm_open(ifname, req, new_flags, arg)
    ccall((:nm_open, libnetmap_user), Ptr{nm_desc}, (Cstring, Ptr{nmreq}, UInt64, Ptr{nm_desc}), ifname, req, new_flags, arg)
end

function nm_close(d)
    ccall((:nm_close, libnetmap_user), Cint, (Ptr{nm_desc},), d)
end

function nm_mmap(d, parent)
    ccall((:nm_mmap, libnetmap_user), Cint, (Ptr{nm_desc}, Ptr{nm_desc}), d, parent)
end

function nm_inject(d, buf, size)
    ccall((:nm_inject, libnetmap_user), Cint, (Ptr{nm_desc}, Ptr{Cvoid}, Csize_t), d, buf, size)
end

function nm_dispatch(d, cnt, cb, arg)
    ccall((:nm_dispatch, libnetmap_user), Cint, (Ptr{nm_desc}, Cint, nm_cb_t, Ptr{u_char}), d, cnt, cb, arg)
end

function nm_nextpkt(d, hdr)
    ccall((:nm_nextpkt, libnetmap_user), Ptr{u_char}, (Ptr{nm_desc}, Ptr{nm_pkthdr}), d, hdr)
end
