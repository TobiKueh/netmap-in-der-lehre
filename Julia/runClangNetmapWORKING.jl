
using Clang

#const LIBCLANG_INCLUDE = joinpath(@__DIR__, "..", "deps", "usr", "include", "clang-c") |> normpath
const LIBCLANG_INCLUDE = "/tmp/stephan"
const LIBCLANG_HEADERS = [joinpath(LIBCLANG_INCLUDE, header) for header in readdir(LIBCLANG_INCLUDE) if endswith(header, ".h")]

ctx = DefaultContext()

parse_headers!(ctx,
               LIBCLANG_HEADERS,
               args=["-I", joinpath(LIBCLANG_INCLUDE, "..")],
               includes=vcat(LIBCLANG_INCLUDE, CLANG_INCLUDE),
               )

ctx.libname = "libnetmap_user"
ctx.options["is_function_strictly_typed"] = false
ctx.options["is_struct_mutable"] = false


api_file = joinpath(@__DIR__, "libnetmap_user_api.jl")
api_stream = open(api_file, "w")

trans_unit = first(ctx.trans_units)
root_cursor = getcursor(trans_unit)
    push!(ctx.cursor_stack, root_cursor)
    header = spelling(root_cursor)
    @info "wrapping header: $header ..."

    # loop over all of the child cursors and wrap them, if appropriate.
    ctx.children = children(root_cursor)
    for (i, child) in enumerate(ctx.children)
        child_name = name(child)
        child_header = filename(child)
        ctx.children_index = i
        # choose which cursor to wrap
        startswith(child_name, "__") && continue  # skip compiler definitions
        child_name in keys(ctx.common_buffer) && continue  # already wrapped
        child_header != header && continue  # skip if cursor filename is not in the headers to be wrapped

        wrap!(ctx, child)
    end
    @info "writing $(api_file)"
    println(api_stream, "# Julia wrapper for header: $(basename(header))")
    println(api_stream, "# Automatically generated using Clang.jl\n")
    print_buffer(api_stream, ctx.api_buffer)
    empty!(ctx.api_buffer)  # clean up api_buffer for the next header

close(api_stream)

# write "common" definitions: types, typealiases, etc.
common_file = joinpath(@__DIR__, "netmap_user_common.jl")
open(common_file, "w") do f
    println(f, "# Automatically generated using Clang.jl\n")
    print_buffer(f, dump_to_buffer(ctx.common_buffer))
end

#uncomment the following code to generate dependency and template files
copydeps(dirname(api_file))
print_template(joinpath(dirname(api_file), "LibTemplate2.jl"))
